# maxwell-k/wheels

## Deprecation notice

This GitLab project was used as a package registry to host wheels for
[beancount]. At the time of writing the latest release of beancount is 2.3.6,
see this [pull request]. GitHub actions are now [configured] to build wheels
using `cibuildwheel`. The relevant changes were in the following two pull
requests: [1] and [2]. The [files for download] on PyPI include "manylinux"
wheels for Python 3.11. This repository is no longer required.

[files for download]: https://pypi.org/project/beancount/2.3.6/#files
[beancount]: https://github.com/beancount/beancount
[pull request]: https://github.com/beancount/beancount/pull/782
[configured]:
  https://github.com/beancount/beancount/blob/master/.github/workflows/wheels.yaml#L34
[1]: https://github.com/beancount/beancount/pull/772
[2]: https://github.com/beancount/beancount/pull/773

---

Checkout `beancount` 2.3.5:

    git clone --branch 2.3.5 https://github.com/beancount/beancount \
    && cd beancount

Build wheels suitable for Alpine Linux edge and Fedora 35 [^1]:

    export CIBW_BUILD="cp310-manylinux_x86_64 cp310-musllinux_x86_64" \
    && pipx run cibuildwheel --platform linux

Create a deploy token for this project.

Complete `~/.pypirc` following the [GitLab documentation].

[gitlab documentation]:
  https://docs.gitlab.com/ee/user/packages/pypi_repository/#authenticate-with-a-deploy-token

Upload the packages:

    pipx run twine upload --repository maxwell-k/wheels wheelhouse/*

Use these packages to install `beancount` and `fava`:

    args='--extra-index-url https://gitlab.com/api/v4/projects/34083406/packages/pypi/simple' \
    && pipx install --pip-args "$args" fava \
    && pipx inject --include-apps fava beancount

## Dependencies

- [pipx](https://github.com/pypa/pipx)
- [Docker](https://www.docker.com/)

## Footnotes

[^1]:
    `Docker version 18.09.7, build 2d0083d` produces both wheels. Emulating
    docker with `podman version 3.4.4` fails with
    `Error: unable to start container ✂: workdir "/project" does not exist on container ✂`
